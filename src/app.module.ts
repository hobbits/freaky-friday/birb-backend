import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommentModule } from './comment/comment.module';
import { FriendlistModule } from './friendlist/friendlist.module';
import { ProfileModule } from './profile/profile.module';
import { PromptModule } from './prompt/prompt.module';
import { TopicModule } from './topic/topic.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    TopicModule,
    PromptModule,
    ProfileModule,
    CommentModule,
    FriendlistModule,
  ],
})
export class AppModule {}
