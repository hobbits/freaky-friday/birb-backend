import { Profile } from "src/profile/profile.entity";

export type FriendRequest_Status =
  | 'not-sent'
  | 'pending'
  | 'accepted'
  | 'declined'
  | 'waiting-for-current-user-response';

export interface FriendRequestStatus {
  status?: FriendRequest_Status;
}

export interface FriendRequest {
  id?: number;
  creator?: Profile;
  receiver?: Profile;
  status?: FriendRequest_Status;
}