import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Friendlist } from './friendlist.entity';

@Injectable()
export class FriendlistService {
  constructor(
    @InjectRepository(Friendlist)
    private friendlistRepository: Repository<Friendlist>,
  ) {}

  findAll(): Promise<Friendlist[]> {
    return this.friendlistRepository.find();
  }
}
