import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Friendlist } from './friendlist.entity';
import { FriendlistService } from './friendlist.service';

@ApiTags('friendlist')
@Controller('friendlist')
export class FriendlistController {
  constructor(private readonly friendlistService: FriendlistService) {}

  @Get()
  findALl(): Promise<Friendlist[]> {
    return this.friendlistService.findAll();
  }
}
