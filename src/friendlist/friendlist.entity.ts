import { Profile } from 'src/profile/profile.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { FriendRequest_Status } from './friendlist.interface';

@Entity()
export class Friendlist {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Profile, (profile) => profile.sentFriendRequests)
  creator: Profile;

  @ManyToOne(() => Profile, (profile) => profile.receivedFriendRequests)
  receiver: Profile;

  @Column()
  status: FriendRequest_Status;
}
