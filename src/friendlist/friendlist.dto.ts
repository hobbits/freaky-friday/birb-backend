import { ApiProperty } from '@nestjs/swagger';
import { Profile } from 'src/profile/profile.entity';

export class FriendlistDto {
  @ApiProperty({ type: [Profile] })
  readonly friendRequester: [Profile];

  @ApiProperty({ type: [Profile] })
  readonly friendAssignee: [Profile];
}
