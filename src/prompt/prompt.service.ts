import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PromptDto } from './prompt.dto';
import { Prompt } from './prompt.entity';

@Injectable()
export class PromptService {
  constructor(
    @InjectRepository(Prompt)
    private promptRepository: Repository<Prompt>,
  ) {}

  findAll(): Promise<Prompt[]> {
    return this.promptRepository.find({ relations: ['comments', 'topic'] });
  }

  async findRandom(): Promise<Prompt> {
    try {
      const record = await this.promptRepository
        .createQueryBuilder('prompt')
        .orderBy('RAND ()')
        .limit(1)
        .getOne();
      return record;
    } catch (error) {
      console.log('Error: ', error);
    }
  }

  findOne(id: string): Promise<Prompt> {
    return this.promptRepository.findOne(id, { relations: ['comments'] });
  }

  create(prompt: PromptDto): Promise<Prompt> {
    return this.promptRepository.save(prompt);
  }
}
