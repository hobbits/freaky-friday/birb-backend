import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Comment } from 'src/comment/comment.entity';
import { Topic } from 'src/topic/topic.entity';
@Entity()
export class Prompt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @ManyToOne(() => Topic, (topic) => topic.prompts)
  topic: Topic;

  @OneToMany(() => Comment, (comment) => comment.prompt)
  comments: Comment[];
}
