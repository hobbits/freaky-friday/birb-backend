import { ApiProperty } from '@nestjs/swagger';
import { Comment } from 'src/comment/comment.entity';
import { Topic } from 'src/topic/topic.entity';

export class PromptDto {
  @ApiProperty({ required: true, nullable: false })
  readonly title: string;

  @ApiProperty({ required: true, nullable: false })
  readonly description: string;

  @ApiProperty({ type: Topic })
  readonly topic: Topic;

  @ApiProperty({ type: [Comment] })
  readonly comments: Comment[];
}
