import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { PromptDto } from './prompt.dto';
import { Prompt } from './prompt.entity';
import { PromptService } from './prompt.service';

@ApiTags('prompt')
@Controller('prompt')
export class PromptController {
  constructor(private readonly promptService: PromptService) {}

  @Get()
  findAll(): Promise<Prompt[]> {
    return this.promptService.findAll();
  }

  @Get('/random')
  async findRandom(): Promise<Prompt> {
    return await this.promptService.findRandom();
  }

  @Get(':id')
  @ApiParam({ name: 'id', required: true })
  findOne(@Param('id') id: string): Promise<Prompt> {
    return this.promptService.findOne(id);
  }

  @Post()
  create(@Body() prompt: PromptDto): Promise<Prompt> {
    return this.promptService.create(prompt);
  }
}
