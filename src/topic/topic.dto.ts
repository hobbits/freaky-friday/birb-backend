import { ApiProperty } from '@nestjs/swagger';

export class TopicDto {
  @ApiProperty({ required: true, nullable: false })
  readonly value: string;
}
