import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Prompt } from 'src/prompt/prompt.entity';

@Entity()
export class Topic {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  value: string;

  @OneToMany(() => Prompt, (prompt) => prompt.topic)
  prompts: Prompt[];
}
