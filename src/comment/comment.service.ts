import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CommentDto } from './comment.dto';
import { Comment } from './comment.entity';

@Injectable()
export class CommentService {
  constructor(
    @InjectRepository(Comment)
    private commentRepository: Repository<Comment>,
  ) {}

  findAll(): Promise<Comment[]> {
    return this.commentRepository.find({ relations: ['prompt', 'profile'] });
  }

  findOne(id: string): Promise<Comment> {
    return this.commentRepository.findOne(id);
  }

  create(comment: CommentDto): Promise<Comment> {
    return this.commentRepository.save(comment);
  }
}
