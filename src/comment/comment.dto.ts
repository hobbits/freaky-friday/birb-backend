import { ApiProperty } from '@nestjs/swagger';
import { Profile } from 'src/profile/profile.entity';
import { Prompt } from 'src/prompt/prompt.entity';

export class CommentDto {
  @ApiProperty({ required: true, nullable: false })
  readonly value: string;

  @ApiProperty({ type: Prompt })
  readonly prompt: Prompt;

  @ApiProperty({ type: Profile })
  readonly profile: Profile;
}
