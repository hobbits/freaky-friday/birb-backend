import { Profile } from 'src/profile/profile.entity';
import { Prompt } from 'src/prompt/prompt.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Comment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  readonly value: string;

  @ManyToOne(() => Prompt, (prompt) => prompt.comments)
  prompt: Prompt;

  @OneToOne(() => Profile)
  @JoinColumn()
  profile: Profile;
}
