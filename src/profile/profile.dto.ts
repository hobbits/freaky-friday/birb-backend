import { ApiProperty } from '@nestjs/swagger';

export class ProfileDto {
  @ApiProperty({ required: true, nullable: false })
  readonly displayname: string;

  @ApiProperty({ required: false })
  readonly status: string;

  @ApiProperty({ required: false })
  readonly image: string;
}
