import { Friendlist } from 'src/friendlist/friendlist.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Profile {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  displayname: string;

  @Column()
  status: string;

  @Column()
  image: string;

  @OneToMany(() => Friendlist, (friendList) => friendList.creator)
  sentFriendRequests: Friendlist[];

  @OneToMany(() => Friendlist, (friendList) => friendList.receiver)
  receivedFriendRequests: Friendlist[];
}
