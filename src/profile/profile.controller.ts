import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { ProfileDto } from './profile.dto';
import { Profile } from './profile.entity';
import { ProfileService } from './profile.service';

@ApiTags('profile')
@Controller('profile')
export class ProfileController {
  constructor(private readonly profileService: ProfileService) {}

  @Get()
  findAll(): Promise<Profile[]> {
    return this.profileService.findAll();
  }

  @Get(':id')
  @ApiParam({ name: 'id', required: true })
  findOne(@Param('id') id: string): Promise<Profile> {
    return this.profileService.findOne(id);
  }

  @Post()
  create(@Body() profile: ProfileDto): Promise<Profile> {
    return this.profileService.create(profile);
  }

  @Get(':id/friends')
  @ApiParam({ name: 'id', required: true })
  getFriends(@Param('id') id: string): Observable<Profile[]> {
    return this.profileService.getFriends(id);
  }
}
