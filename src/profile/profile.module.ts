import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Friendlist } from 'src/friendlist/friendlist.entity';
import { ProfileController } from './profile.controller';
import { Profile } from './profile.entity';
import { ProfileService } from './profile.service';

@Module({
  imports: [TypeOrmModule.forFeature([Profile, Friendlist])],
  providers: [ProfileService],
  controllers: [ProfileController],
})
export class ProfileModule {}
