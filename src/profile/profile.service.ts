import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Friendlist } from 'src/friendlist/friendlist.entity';
import { Repository } from 'typeorm';
import { ProfileDto } from './profile.dto';
import { Profile } from './profile.entity';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class ProfileService {
  constructor(
    @InjectRepository(Profile)
    private profileRepository: Repository<Profile>,
    @InjectRepository(Friendlist)
    private friendListRepository: Repository<Profile>,
  ) {}

  findAll(): Promise<Profile[]> {
    return this.profileRepository.find();
  }

  findOne(id: string): Promise<Profile> {
    return this.profileRepository.findOne(id);
  }

  create(profile: ProfileDto): Promise<Profile> {
    return this.profileRepository.save(profile);
  }

  getFriends(id: string): Observable<Profile[]> {
    const currentUser = this.findOne(id);

    return from(
      this.friendListRepository.find({
        where: [
          { creator: currentUser, status: 'accepted' },
          { receiver: currentUser, status: 'accepted' },
        ],
        relations: ['creator', 'receiver'],
      }),
    ).pipe(
      switchMap((friends: any[]) => {
        const userIds: number[] = [];

        friends.forEach((friend) => {
          if (friend.creator.id === 1) {
            userIds.push(friend.receiver.id);
          } else if (friend.receiver.id === 1) {
            console.log(friend.receiver);
            userIds.push(friend.creator.id);
          }
        });
        return from(this.profileRepository.findByIds(userIds));
      }),
    );
  }
}
